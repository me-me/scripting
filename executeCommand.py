#!/usr/bin/env python3

import subprocess
import optparse
import re

parser = optparse.OptionParser()


def getArgs():
    parser.add_option("-i", "--interface", dest="interface", help="Interface to change its Mac address.")
    parser.add_option("-m", "--Mac_address", dest="Mac_address", help="The new Mac address to replace.")
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify an interface, use --help for more information.")
    elif not options.Mac_address:
        parser.error("[-] Please specify a Mac address, use --help for more information.")
    return options;


def changeMacAddress (interface, new_address):
    print("[+] changing Mac address2 of " + new_address + interface + " to new Mac address...")
    # subprocess.call("ifconfig wlan0 down", shell=True)
    # subprocess.call("ifconfig wlan0 hw ether " + new_address, shell=True)
    # subprocess.call("ifconfig wlan0 up", shell=True)

def get_current_macAddress(interface):
    ifconfig_results = subprocess.check_output(["ifconfig", interface])
    mac_address_search_results = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", ifconfig_results)

    if mac_address_search_results:
        print("[+] Mac address : " + mac_address_search_results.group(0))
        return mac_address_search_results.group(0)
    else:
        print("[-] Could not read Mac address.")


options = getArgs()

get_current_macAddress(options.interface)
changeMacAddress(options.interface, options.Mac_address)
